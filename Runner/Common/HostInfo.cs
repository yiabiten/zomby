﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner.Common
{
    class HostInfo
    {
        public static readonly HostInfo Instance = new HostInfo();
        public HostInfo()
        {
            //Init vars here
        }
        private static readonly string computername;
        private static readonly string username;
        private static readonly string os;
        private static readonly string uniqueIdentifier;
    }
}
