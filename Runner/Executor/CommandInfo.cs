﻿namespace Runner.Executor
{
    class CommandInfo
    {
        readonly string query;
        readonly string workingDirectory;
        public CommandInfo(string query, string workingDirectory)
        {
            this.query = query;
            this.workingDirectory = workingDirectory;
        }
    }
}
