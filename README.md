[![Build status](https://ci.appveyor.com/api/projects/status/pjoc5s637al5el13?svg=true)](https://ci.appveyor.com/project/Yiabiten/zomby)
# Zomby

A pseudo-spy: keylogger & remote command execution on containing machines + auto-updating binaries

### How do I get set up? ###

* Language : C#
* .Net framework : <to be determined>
* Fork - clone -  edit & open a pull request

### Who do I talk to? ###

* Repo owner (@yiabiten)